const fs = require('fs');
const { exec } = require('child_process');
const dotenv = require('dotenv');
const path = require('path');

// Load .env.local variables
dotenv.config({ path: ['.env', '.env.local'] });

const projectId = process.env.NEXT_PUBLIC_SUPABASE_PROJECT_ID;

if (!projectId) {
  console.error("Error: NEXT_PUBLIC_SUPABASE_PROJECT_ID is not set. Make sure it's defined in .env.local.");
  process.exit(1);
}

// Ensure the target directory exists
const dirPath = path.join(__dirname, 'src', 'types');
if (!fs.existsSync(dirPath)) {
  fs.mkdirSync(dirPath, { recursive: true });
}

// Generate types using the project_id from the environment variable
const command = `npx supabase gen types typescript --project-id ${projectId} > ${path.join(dirPath, 'database.ts')}`;

exec(command, (error, stdout, stderr) => {
  if (error) {
    console.error(`Error: Failed to generate TypeScript types. ${stderr}`);
    process.exit(1);
  }
  console.log('TypeScript types generated successfully.');
});

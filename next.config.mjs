/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    minimumCacheTTL: 60,
    unoptimized: true,
  },
};

export default nextConfig;

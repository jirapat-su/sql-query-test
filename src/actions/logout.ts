'use server';

import { supabaseServer } from '@/lib/supabase/server';
import { revalidatePath } from 'next/cache';

export const logoutAction = async (currentPath?: string) => {
  const supabase = supabaseServer();

  const { error } = await supabase.auth.signOut();
  if (error) {
    throw error;
  }

  revalidatePath(currentPath || '/', 'layout');
  return;
};

'use server';

import { supabaseServer } from '@/lib/supabase/server';
import { revalidatePath } from 'next/cache';

type OnboardingSubmitActionProps = {
  first_name: string;
  is_exchange_student?: boolean | undefined;
  last_name: string;
  middle_name?: string | undefined;
  student_id: string;
};

export const onboardingSubmitAction = async (formData: OnboardingSubmitActionProps) => {
  const supabase = supabaseServer();

  const {
    data: { user },
    error,
  } = await supabase.auth.getUser();

  if (error) {
    console.error('Error fetching user:', error);
    throw error;
  }

  const { error: updateError } = await supabase
    .from('users')
    .update({
      is_request_onboarding: false,
      ...formData,
    })
    .eq('user_id', user?.id || '');

  if (updateError) {
    console.error('Error updating user:', updateError);
    throw updateError;
  }

  revalidatePath('/', 'layout');
  return;
};

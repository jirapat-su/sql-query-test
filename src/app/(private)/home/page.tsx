import LogoutButton from '../logout-button';

const HomePage = () => {
  return (
    <div>
      <h1>Home Page</h1>

      <LogoutButton />
    </div>
  );
};

export default HomePage;

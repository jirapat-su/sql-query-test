import OnboardingForm from '@/components/onboarding-form';
import { supabaseServer } from '@/lib/supabase/server';
import { redirect } from 'next/navigation';
import { Fragment } from 'react';

type PrivateLayoutProps = {
  children: Readonly<React.ReactNode>;
};

const PrivateLayout: React.FC<PrivateLayoutProps> = async ({ children }) => {
  const supabase = supabaseServer();
  const {
    data: { user },
    error,
  } = await supabase.auth.getUser();

  if (error) {
    console.error('Error authenticating user:', error);
    return redirect('/login');
  }

  const userInfo = await supabase
    .from('users')
    .select('*')
    .eq('user_id', user?.id ?? '')
    .limit(1)
    .single();

  if (userInfo.error) {
    console.error('Error getting user info:', userInfo.error);
    return redirect('/login');
  }

  if (!userInfo.data?.is_teacher && userInfo.data?.is_request_onboarding) {
    return <OnboardingForm />;
  }

  return <Fragment>{children}</Fragment>;
};

export default PrivateLayout;

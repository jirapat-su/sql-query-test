'use client';

import { logoutAction } from '@/actions/logout';
import { Button } from '@/components/ui/button';
import { Loader2 } from 'lucide-react';
import { useTransition } from 'react';

const LogoutButton = () => {
  const [isPending, startTransition] = useTransition();

  const handleLogout = () => {
    startTransition(async () => {
      try {
        await logoutAction(location.pathname);
      } catch (error) {
        console.error('Error logging out:', error);
      }
    });
  };

  return (
    <Button
      disabled={isPending}
      onClick={handleLogout}
    >
      {isPending && <Loader2 className='mr-2 h-4 w-4 animate-spin' />} ออกจากระบบ
    </Button>
  );
};

export default LogoutButton;

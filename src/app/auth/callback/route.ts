import { supabaseServer } from '@/lib/supabase/server';
import { NextRequest, NextResponse } from 'next/server';

const redirectWithError = (origin: string, message: string) => {
  const params = new URLSearchParams();
  params.append('error_message', encodeURIComponent(message));
  return NextResponse.redirect(`${origin}?${params.toString()}`);
};

export const GET = async (request: NextRequest) => {
  const requestUrl = new URL(request.url);
  const code = requestUrl.searchParams.get('code');
  const errorMessage = `Sorry, we weren't able to log you in. Please try again.`;

  if (!code) {
    console.error('Auth Callback', "Missing 'code' parameter in query string");
    return redirectWithError(requestUrl.origin, errorMessage);
  }

  const supabase = supabaseServer();
  const { data, error } = await supabase.auth.exchangeCodeForSession(code);

  if (error) {
    console.error('Auth Callback', 'Error exchanging code for session', error);
    return redirectWithError(requestUrl.origin, errorMessage);
  }

  console.log('Auth Callback', 'User logged in', data);
  return NextResponse.redirect(`${requestUrl.origin}/home`);
};

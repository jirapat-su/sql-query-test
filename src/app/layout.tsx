import type { Metadata, Viewport } from 'next';

import { Sarabun } from 'next/font/google';

import './globals.css';

const sarabun = Sarabun({
  display: 'auto',
  preload: true,
  style: ['normal', 'italic'],
  subsets: ['latin', 'thai'],
  weight: ['100', '200', '300', '400', '500', '600', '700', '800'],
});

export const metadata: Metadata = {
  alternates: {
    canonical: new URL('https://sql-query-test.vercel.app/'),
  },
  authors: [{ name: 'Jirapat Sukhonthapong', url: 'https://www.linkedin.com/in/jirapat-su/' }],
  category: 'Software, Database, SQL, Development Tools',
  creator: 'Jirapat Sukhonthapong',
  description:
    'A website that can check the correctness of SQL commands and provide suggestions in case the system detects incorrect SQL command writing.',
  keywords: 'SQL, Database, Command Checking, Development Tools, Query',
  metadataBase: new URL('https://sql-query-test.vercel.app/'),
  publisher: 'Jirapat Sukhonthapong',
  referrer: 'origin-when-cross-origin',
  robots: {
    follow: true,
    googleBot: {
      follow: true,
      index: true,
      'max-image-preview': 'large',
      'max-snippet': -1,
      'max-video-preview': -1,
      noimageindex: false,
    },
    index: true,
    nocache: true,
  },
  title: {
    absolute: 'SQL Command Checking System',
    default: 'SQL Command Checking System',
    template: '%s | SQL Command Checking System',
  },
  verification: {
    google: 'google',
    other: {
      me: ['https://www.linkedin.com/in/jirapat-su/'],
    },
    yahoo: 'yahoo',
    yandex: 'yandex',
  },
};

export const viewport: Viewport = {
  initialScale: 1,
  maximumScale: 2,
  minimumScale: 1,
  themeColor: [
    { color: '#121212', media: '(prefers-color-scheme: dark)' },
    { color: '#ffffff', media: '(prefers-color-scheme: light)' },
  ],
  width: 'device-width',
};

type RootLayoutProps = {
  children: Readonly<React.ReactNode>;
};

const RootLayout: React.FC<RootLayoutProps> = ({ children }) => {
  return (
    <html
      lang='en'
      suppressHydrationWarning
    >
      <body className={sarabun.className}>{children}</body>
    </html>
  );
};

export default RootLayout;

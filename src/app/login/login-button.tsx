'use client';

import { Button } from '@/components/ui/button';
import { supabaseClient } from '@/lib/supabase/client';

const LoginButton = () => {
  const supabase = supabaseClient();

  const handleLogin = async () => {
    const { error } = await supabase.auth.signInWithOAuth({
      options: { redirectTo: `${location.origin}/auth/callback` },
      provider: 'google',
    });

    if (error) {
      console.error('Error logging in:', error);
    }
  };

  return (
    <Button
      className='block'
      onClick={handleLogin}
    >
      ลงชื่อเข้าใช้งานด้วยบัญชี Google ของคุณ
    </Button>
  );
};

export default LoginButton;

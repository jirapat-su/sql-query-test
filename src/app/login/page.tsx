import { AspectRatio } from '@/components/ui/aspect-ratio';
import { Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle } from '@/components/ui/card';
import ThemeToggle from '@/components/ui/theme-toggle';
import Image from 'next/image';

import LoginButton from './login-button';

const LoginPage = async () => {
  return (
    <div className='min-h-svh w-full lg:flex lg:items-center lg:justify-center'>
      <div className='w-full p-4 lg:max-w-xl'>
        <Card>
          <CardHeader className='relative flex items-center justify-center'>
            <div className='mb-4 w-[160px]'>
              <AspectRatio ratio={11 / 9}>
                <Image
                  alt='CPE Logo'
                  fill
                  priority
                  src='/CPE_LOGO.webp'
                />
              </AspectRatio>
            </div>

            <CardTitle>ลงชื่อเข้าใช้งาน</CardTitle>
            <CardDescription className='text-md'>ระบบตรวจสอบคำสั่งเอสคิวแอล</CardDescription>

            <div className='absolute right-4 top-2 z-10'>
              <ThemeToggle />
            </div>
          </CardHeader>

          <CardContent>
            <div className='mx-auto max-w-fit'>
              <LoginButton />
            </div>
          </CardContent>

          <CardFooter className='justify-center'>
            <p className='text-balance text-center text-sm text-muted-foreground'>
              หากไม่ใช่คอมพิวเตอร์ของคุณ ให้ใช้โหมดผู้มาเยือนเพื่อลงชื่อเข้าใช้แบบส่วนตัว
            </p>
          </CardFooter>
        </Card>

        <div className='pt-6'>
          <p className='text-balance text-center text-muted-foreground'>
            {`Copyright © ${new Date().getFullYear()} สาขาวิชาวิศวกรรมคอมพิวเตอร์ มหาวิทยาลัยเทคโนโลยีราชมงคลอีสาน
              นครราชสีมา`}
          </p>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;

import { supabaseServer } from '@/lib/supabase/server';
import { redirect } from 'next/navigation';

const IndexPage = async () => {
  const supabase = supabaseServer();
  const { error } = await supabase.auth.getUser();

  if (error) {
    console.error('Private Layout', 'Error getting user', error);
    return redirect('/login');
  }

  return redirect('/home');
};

export default IndexPage;

import React from 'react';

import ThemeProvider from './theme-provider';

type RootTemplateProps = {
  children: Readonly<React.ReactNode>;
};

const RootTemplate: React.FC<RootTemplateProps> = ({ children }) => {
  return (
    <ThemeProvider
      attribute='class'
      defaultTheme='dark'
      disableTransitionOnChange
      enableSystem
    >
      {children}
    </ThemeProvider>
  );
};

export default RootTemplate;

'use client';

import { onboardingSubmitAction } from '@/actions/onboarding-submit';
import { Button } from '@/components/ui/button';
import { Checkbox } from '@/components/ui/checkbox';
import { Form, FormControl, FormDescription, FormField, FormItem, FormLabel, FormMessage } from '@/components/ui/form';
import { Input } from '@/components/ui/input';
import { zodResolver } from '@hookform/resolvers/zod';
import { Loader2 } from 'lucide-react';
import { useTransition } from 'react';
import { useForm } from 'react-hook-form';
import { z } from 'zod';

import { OnboardingFormSchema } from './schema';

type OnboardingFormProps = z.infer<typeof OnboardingFormSchema>;

const OnboardingForm = () => {
  const [isPending, startTransition] = useTransition();
  const form = useForm<OnboardingFormProps>({
    defaultValues: {
      first_name: '',
      is_exchange_student: false,
      last_name: '',
      middle_name: '',
      student_id: '',
    },
    mode: 'onBlur',
    resolver: zodResolver(OnboardingFormSchema),
  });

  const onSubmit = (data: OnboardingFormProps) => {
    startTransition(async () => {
      try {
        await onboardingSubmitAction(data);
      } catch (error) {
        console.error('Error submitting onboarding form:', error);
      }
    });
  };

  return (
    <div className='flex min-h-svh flex-col'>
      <div className='flex-1 p-4'>
        <div className='mx-auto mt-8 w-full max-w-[500px] rounded-lg border p-4'>
          <h1 className='text-center text-2xl font-bold'>ข้อมูลส่วนตัว</h1>

          <Form {...form}>
            <form
              className='w-full space-y-6'
              onSubmit={form.handleSubmit(onSubmit)}
            >
              <FormField
                control={form.control}
                name='is_exchange_student'
                render={({ field }) => (
                  <FormItem>
                    <FormControl>
                      <div className='items-top mt-6 flex space-x-2'>
                        <Checkbox
                          checked={field.value}
                          id='terms1'
                          onCheckedChange={field.onChange}
                        />
                        <div className='grid gap-1.5 leading-none'>
                          <label
                            className='text-sm font-medium leading-none peer-disabled:cursor-not-allowed peer-disabled:opacity-70'
                            htmlFor='terms1'
                          >
                            Please select if you are an exchange student.
                          </label>
                          <p className='text-sm text-muted-foreground'>โปรดเลือกหากคุณเป็นนักศึกษาแลกเปลี่ยน</p>
                        </div>
                      </div>
                    </FormControl>
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name='student_id'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>รหัสนักศึกษา</FormLabel>
                    <FormControl>
                      <Input
                        placeholder='Student ID'
                        {...field}
                      />
                    </FormControl>
                    <FormDescription>โปรดกรอกรหัสนักศึกษาพร้อมเครื่องหมายขีดกลาง (-)</FormDescription>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name='first_name'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>ชื่อ</FormLabel>
                    <FormControl>
                      <Input
                        placeholder='First Name'
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <FormField
                control={form.control}
                name='middle_name'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>ชื่อกลาง (ถ้ามี)</FormLabel>
                    <FormControl>
                      <Input
                        placeholder='Middle Name (Optional)'
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
                rules={{ required: false }}
              />

              <FormField
                control={form.control}
                name='last_name'
                render={({ field }) => (
                  <FormItem>
                    <FormLabel>นามสกุล</FormLabel>
                    <FormControl>
                      <Input
                        placeholder='Last Name'
                        {...field}
                      />
                    </FormControl>
                    <FormMessage />
                  </FormItem>
                )}
              />

              <div className='flex justify-end'>
                <Button
                  disabled={isPending}
                  type='submit'
                >
                  {isPending && <Loader2 className='mr-2 h-4 w-4 animate-spin' />} บันทึก
                </Button>
              </div>
            </form>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default OnboardingForm;

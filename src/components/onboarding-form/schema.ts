import {
  ENGLISH_REGEX,
  ENGLISH_REGEX_WITH_SPACE,
  STUDENT_ID_REGEX,
  THAI_REGEX,
  THAI_REGEX_WITH_SPACE,
} from '@/constants/regex';
import { supabaseClient } from '@/lib/supabase/client';
import { z } from 'zod';

export const OnboardingFormSchema = z
  .object({
    first_name: z.string().min(1, {
      message: 'กรุณากรอกข้อมูล',
    }),
    is_exchange_student: z.boolean().default(false),
    last_name: z.string().min(1, {
      message: 'กรุณากรอกข้อมูล',
    }),
    middle_name: z.string().optional(),
    student_id: z
      .string()
      .min(13, {
        message: 'รหัสนักศึกษาต้องมีอักขระอย่างน้อย 13 ตัว รวมเครื่องหมายขีดกลาง (-)',
      })
      .regex(STUDENT_ID_REGEX, {
        message: 'รหัสนักศึกษาต้องอยู่ในรูปแบบ 5717340320xx-x หรือ 601723101xx-x',
      })
      .superRefine(async (val, ctx) => {
        const supabase = supabaseClient();
        const { data, error } = await supabase.from('users').select('*').eq('student_id', val).limit(1);

        if (error) {
          ctx.addIssue({
            code: z.ZodIssueCode.custom,
            message: 'เกิดข้อผืดพลาดในการตรวจสอบรหัสนักศึกษา กรุณาลองใหม่อีกครั้ง',
          });

          return false;
        }

        if (data.length > 0) {
          ctx.addIssue({
            code: z.ZodIssueCode.custom,
            message: 'รหัสนักศึกษานี้ถูกใช้แล้ว กรุณากรอกรหัสนักศึกษาใหม่',
          });

          return false;
        }

        return true;
      }),
  })
  .superRefine((val, ctx) => {
    const FIRSTNAME_ENGLISH_TEST = ENGLISH_REGEX.test(val.first_name);
    const FIRSTNAME_THAI_TEST = THAI_REGEX.test(val.first_name);

    if (val.is_exchange_student) {
      if (!FIRSTNAME_ENGLISH_TEST) {
        ctx.addIssue({
          code: z.ZodIssueCode.custom,
          message: 'Please fill first name in English without numbers, special characters, and spaces.',
          path: ['first_name'],
        });

        return z.NEVER;
      }

      return true;
    }

    if (!FIRSTNAME_THAI_TEST) {
      ctx.addIssue({
        code: z.ZodIssueCode.custom,
        message: 'กรุณากรอกชื่อเป็นภาษาไทยโดยไม่มีตัวเลข อักขระพิเศษ และช่องว่าง',
        path: ['first_name'],
      });

      return z.NEVER;
    }

    return true;
  })
  .superRefine((val, ctx) => {
    if (val.middle_name!.length === 0) return true;

    const MIDDLENAME_ENGLISH_TEST = ENGLISH_REGEX.test(val.middle_name!);
    const MIDDLENAME_THAI_TEST = THAI_REGEX.test(val.middle_name!);

    if (val.is_exchange_student) {
      if (!MIDDLENAME_ENGLISH_TEST) {
        ctx.addIssue({
          code: z.ZodIssueCode.custom,
          message: 'Please fill middle name in English without numbers, special characters, and spaces.',
          path: ['middle_name'],
        });

        return z.NEVER;
      }

      return true;
    }

    if (!MIDDLENAME_THAI_TEST) {
      ctx.addIssue({
        code: z.ZodIssueCode.custom,
        message: 'กรุณากรอกชื่อกลางเป็นภาษาไทยโดยไม่มีตัวเลข อักขระพิเศษ และช่องว่าง',
        path: ['middle_name'],
      });

      return z.NEVER;
    }

    return true;
  })
  .superRefine((val, ctx) => {
    const LASTNAME_ENGLISH_TEST = ENGLISH_REGEX_WITH_SPACE.test(val.last_name);
    const LASTNAME_THAI_TEST = THAI_REGEX_WITH_SPACE.test(val.last_name);

    if (val.is_exchange_student) {
      if (!LASTNAME_ENGLISH_TEST) {
        ctx.addIssue({
          code: z.ZodIssueCode.custom,
          message: 'Please fill last name in English without numbers and special characters.',
          path: ['last_name'],
        });

        return z.NEVER;
      }

      return true;
    }

    if (!LASTNAME_THAI_TEST) {
      ctx.addIssue({
        code: z.ZodIssueCode.custom,
        message: 'กรุณากรอกนามสกุลเป็นภาษาไทยโดยไม่มีตัวเลขและอักขระพิเศษ',
        path: ['last_name'],
      });

      return z.NEVER;
    }

    return true;
  });

export const THAI_REGEX = /^(?:[ก-๏]+)$/;
export const ENGLISH_REGEX = /^(?:[a-zA-Z]+)$/;
export const THAI_REGEX_WITH_SPACE = /^(?:[ก-๏]+(?: [ก-๏]+)?)$/;
export const ENGLISH_REGEX_WITH_SPACE = /^(?:[a-zA-Z]+(?: [a-zA-Z]+)?)$/;
export const STUDENT_ID_REGEX = /^(?:\d{11}|\d{12})-\d{1}$/;
